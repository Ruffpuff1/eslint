# eslint-config-ruffpuff
Eslint configuration for Ruff's projects.

## Installation
```
npm install --dev eslint eslint-config-ruffpuff
```

.eslintrc:
```json
{
	"extends": "ruffpuff"
}
```